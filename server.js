import sqlite3 from 'sqlite3'
import express from 'express'
import cors from 'cors'
import {createRequire} from 'module'
const require = createRequire(import.meta.url)
const config = require('./config.json');
import {get_GTM_timezone, get_rows, logMessage} from "./functions.mjs";

const app = express()

let main_message = config.MAIN_MESSAGE

export const LOG_FILE = config.LOG_FILE

const time_zone = get_GTM_timezone()

app.use(cors({
    origin: '*'
}));

app.use(express.json())

let db = new sqlite3.Database(config.DBSOURCE, (err) => {
    if (err) {
        // Cannot open database
        logMessage(err.message, "ERROR")
        throw err
    } else {
        logMessage('Connected to the SQLite database.', "INFO")
    }
});

app.get('/', (request, response) => {
    logMessage(request.data, "INFO")
    response.json({
        "message": "success"
    })
})

app.post('/', (request, response) => {

    const logg_mesage = request.method + ' ' + request.url + ' ' + JSON.stringify(request.body).replace(/"/g, "'")
    if (request.body["type"] === "all") {
        const query = "SELECT * FROM data"
        db.all(query, (err, rows) => get_rows(err, rows, response, main_message, logg_mesage))
    } else if (request.body["type"] === "range") {
        const date_from = new Date(request.body["from"] + ` ${time_zone}`)
        const date_to = new Date(request.body["to"] + ` ${time_zone}`)

        const query = "SELECT * FROM data WHERE timestamp BETWEEN ? AND ?"
        let params = [date_from.getTime() / 1000, date_to.getTime() / 1000]

        db.all(query, params, (err, rows) => get_rows(err, rows, response, main_message, logg_mesage))
    } else if (request.body["type"] === "times") {
        const values = request.body["times"]
        const times = values.map(item => new Date(item + ` ${time_zone}`).getTime() / 1000)
        const query = `SELECT *
                       FROM data
                       WHERE timestamp IN (${times})`

        db.all(query, (err, rows) => get_rows(err, rows, response, logg_mesage))
    } else if (request.body["type"] === "update") {
        const last = new Date(request.body["last"] + ` ${time_zone}`)
        const query = "SELECT * FROM data WHERE timestamp > ?"
        let params = [last.getTime() / 1000]
        db.all(query, params, (err, rows) => get_rows(err, rows, response, main_message, logg_mesage))
    } else {
        logMessage("404 " + request.method + ' ' + request.url + ' ')
        response.status(404).json({"error": "Not found"});
    }
})


app.listen(config.PORT, config.HOST, () => logMessage(`Running Server ${config.MAIN_MESSAGE.data[0].info.name} ${config.MAIN_MESSAGE.data[0].info.ip} on http://${config.HOST}:${config.PORT}`, "INFO"))
