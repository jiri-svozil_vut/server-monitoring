import format from "date-format";
import fs from "fs";
import {LOG_FILE} from './server.js'

function modify_timestamp(rows) {
    return rows.map(item => {
        const cur_date = new Date(item.timestamp * 1000)
        item.timestamp = format("yyyy-MM-ddThh:mm:ss.SSSSSS+0000", cur_date)
        return item
    })
}


export function get_time() {
    const date = new Date()
    const zone_offset = get_timezone(date.getTimezoneOffset())
    return format(`dd/MM/yyyy hh:mm:ss ${zone_offset}`, date)
}

function get_timezone(pure_value){
    const hours = pure_value / 60 * -1
    const padd_hours = hours.toString().padStart(2, '0');
    let out;
    if (pure_value < 0){
        out = `+${padd_hours}00`
    }
    else {
        out = `${padd_hours}00`
    }
    return out
}

export function get_GTM_timezone(){
    const offset = new Date().getTimezoneOffset()
    const hours = offset / 60 * -1

    let out;
    if (offset < 0){
        out = `GMT +${hours}`
    }
    else {
        out = `GMT ${hours}`
    }
    return out
}

export function logMessage(message, log_type) {
    const out_message = `[${get_time()}] - ${log_type} - "${message}"`;
    const log_entry = out_message + '\n';
    console.log(out_message);
    try {
        fs.appendFileSync(LOG_FILE, log_entry);
    }
    catch (err) {
      console.log(`Chyba při zápisu do souboru: ${err}`);
    }


}

export function get_rows(err, rows, response,  main_message, logg_message) {
    if (err) {
        logMessage(err.message, "WARNING")
        response.status(400).json({"error":err.message});
        return;
    }

    let out = main_message
    out.data[0].values = modify_timestamp(rows)
    response.json(out)
    logMessage(`${logg_message} Returned_values: ${rows.length}`, "INFO")
}
