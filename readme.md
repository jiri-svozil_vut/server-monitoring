# Návod na spuštění Server Monitoring Aplikace

Celá aplikace na monitoring se skládá ze dvou částí. 
První částí je nástroj na sběr, ukládání a přenos dat. Tato část se spouští na zařízení, které chceme monitorovat.
Druhou částí je React aplikace pro vizualizaci dat. 

## Postup spuštění nástroje na sběr a přenos dat.

Tato část se skládá z Node.js API (`server.js`) a bash scriptu, který sbírá data (`scripts/server-monitoring.sh`).


### Stažení zdrojových kódů

Nejjednoušší způsob je `git clone https://gitlab.com/jiri-svozil_vut/bachelor_thesis/server-monitoring`. 
Tento příkaz naklonuje potřebné soubory do adresáře `server-monitoring/`. 
Pro následné spuštění scriptů je důlité, kde se tento adresář nachází, proto doporučuji umístění adresáře v adresáři `/var` pod uživatelem `root` (je možné aplikaci provozovat i pod jinými uživateli, ale pro jednoduchost tento návod popisuje spuštění pod uživatelm `root`).
<br>
Příkazy vypadají následovně:
```
sudo su
cd /var'
git clone https://gitlab.com/jiri-svozil_vut/bachelor_thesis/server-monitoring
cd /var/server-monitoring
```

Po úspěšném naklonování adresáře je nutné prvovést inicializační instalaci pořebných nástrojů a balíčků. V tomto případě se jedná o `sqlite3`,`nodejs` a `npm`. 
Toho se dá docílit inicializačním scriptem `scripts/initial_script.sh`. Tento script by měl nastavit vše potřebné. Pokud u nějakého z příkazů script selže, je nutné provést příkazy manuálně.

### Nastavení nezbytných údajů

#### Nastavení scriptu na sběr metrik

Script na sběr a ukládání metrik se nachází v adresáři `/var/server-monitoring/scripts/server-monitoring.sh`. V tomto scriptu je nutné nastavit potřebné informace, které se mohou lišit od každé stanice. 
<br>
Je tedy nutné nástavit: 
<br>
- `INTERFACE=<interface>` - Síťové rozhraní, na kterém bude script zjišťovat `bitrate` a `packetrate`. Nejednouší způsob pro zjištění je příkaz `ifconfig`.
<br>
- `DB_FILE=/var/db.sqlite` - Umístění Databáze - Doporučuji tuto hodnotu ponechat.
<br>
- `LOG_FILE=/var/log/apache2/custom.log` - Umístění souboru Logu Apache - Z tohoto souboru script čte informace o HTTP spojeních.
<br> 
- `HTTP=true`  - Přepínač který scriptu říká jestli zaznamenávat data z Logu Apache. 
U něktrých stanic není žádoucí monitorovat Logy Apache, tím pádem se dá tato hodnota nastavit na `false` a metriky, které závisí na Logu Apache budou mít automaticky hodnoty `0`.
<br>
S nastavením `LOG_FILE=/var/log/apache2/custom.log` se pojí nuntonst přidat speciální formát loggu do nastavení Appache. To se provede následovně

##### Změna Formatu logu Apache

- `vim /etc/apache2/sites-available/000-default.conf` - Otevření konfiguračního souboru Apache.
- Vložení dvou řádků do konfiguračního soboru. První řádek určuje formát logu a druhý definuje soubor, kam se mají logy ukládat, tento soubor by se měl shodovat s proměnou `LOG_FILE` ze souboru `/var/server-monitoring/scripts/server-monitoring.sh`.
```
LogFormat "%h %l %u %t \"%r\" %>s %b %I %O %T %D" custom
CustomLog /var/log/apache2/custom.log custom
```
      
- `systemctl restart apache2` - Restart servisky apache2


#### Nastavení Node.js API pro přenášení dat do webové aplikace
Nastavení node.js aplikace se nachází v souboru `/var/server-monitoring/config.json`. 
V tomto souboru se nachází nastavení jako umístění databáze, logu kam má aplikace loggovat. 
A další natavení jako je port a host adresa.
Dále je nutné zde správně nastavit IP Adresu. 
Tak aby se shodovala s Adresou serveru a adresou nastavenou v React aplikaci. 
(Jedná se o políčko: `MAIN_MESSAGE.data[0].info.ip`) - Toto je opravdu důležité nastavit.

###  Spuštění applikace

#### Spuštění scriptu na sběr metrik

Po nastavení scriptu je možné script spustit `bash /var/server-monitoring/scripts/server-monitoring.sh`. 
(Před samotným spuštěním je ještě nutné přidělit souboru permission na spuštění `chmod +x /var/server-monitoring/scripts/server-monitoring.sh`).


#### Spuštění scriptu na sběr metrik jako servisky.

Z důvodu praktičnosti je na místě pouštět script na pozadí.
```
cat /var/server-monitoring/scripts/service.txt > /lib/systemd/system/server-monitoring.service
systemctl daemon-reload
systemctl enable server-monitoring.service
systemctl start server-monitoring.service
```
Pokud je vše nastaveno správně po zadání příkazu `systemctl status server-monitoring.service` by měl být script aktivní.

### Sputění node.js aplikace
Po nastavení všech parametů možné aplikaci spustit. 

```
cd /var/server-monitoring
chmod +x /var/server-monitoring/server.js
npm install
node server.js
```

#### Sputění node.js aplikace jako servisky

I node.js server se dá spustit jako serviska. Podobně jako v případě scriptu na ukládání metrik.
```
cat /var/server-monitoring/node-service.txt > /lib/systemd/system/server-monitoring-api.service
systemctl daemon-reload
systemctl enable server-monitoring-api.service
systemctl start server-monitoring-api.service
```

### Ověření funkčnosti
**Výše uvedené příkazy z kapitoly Spuštění aplikace lze nahradit spuštěním scriptu `var/server-monitoring/scripts/initial_script.sh`.**

**Pro ověření funčnosti je možné použít následující příkazy:**
```
curl http://<ip_address>:<port>/
curl -X POST http://<ip_address>:<port>/ -H "Content-Type: application/json" -d '{"type": "all"}' 
```
Výslekem prvního příkazu by měla být message:succes. A výsledkem druhého příkazu by měla být zachycená data.
Aplikace také loguje do souboru `/var/server-monitoring/server-monitoring.log`.

## Postup při spuštění React aplikace

Postup nepočítá s nasazením aplikace v produkčním prostředí. Proto tento postup zahrnuje instrukce pro spuštění ve vývojovém režimu na lokálním zařízení. 
### Stažení zdrojových kódů

Nejjednoušší způsob je `git clone https://gitlab.com/jiri-svozil_vut/bachelor_thesis/react_monitoring_app.git`. 

### Nastavení aplikace
V soubru `src/components/ListOfServers.json` je nutné nastavit mintorované servery. Pozor! Ip adresy musí být stejné jako na monitorovanách zařízeních v `config.json` u Node.js API.

### Spuštění aplikace

Aplikace se spouští příkazy `npm start`, v případě prvního spuštění je nutné použít příkaz `npm install`. Pokud na lokálním zařízení není nainstalované Node.js a npm je nutné jej donistalovat.
