[Unit]
Description=Monitoring service
Documentation=https://gitlab.com/jiri-svozil_vut/bachelor_thesis/server-monitoring
# Location: /lib/systemd/system/server-monitoring.service
[Service]
Type=simple
User=root
Group=root
TimeoutStartSec=0
Restart=on-failure
#ExecStartPre=
ExecStart=/var/server-monitoring/scripts/server-monitoring.sh
SyslogIdentifier=Diskutilization
#ExecStop=

[Install]
WantedBy=multi-user.target