#!/bin/bash

INTERFACE=enp0s5
DB_FILE=/var/db.sqlite
LOG_FILE=/var/log/apache2/custom.log
HTTP=true

INTERVAL=1

get_data () {
  # Get now value for HTTP metrics monitoring
  now_date=`date +%d/%b/%Y:%H:%M:%S`
  # Store values of Bytes and packets
  R1=`cat /sys/class/net/$INTERFACE/statistics/rx_bytes`
  T1=`cat /sys/class/net/$INTERFACE/statistics/tx_bytes`

  PR1=`cat /sys/class/net/$INTERFACE/statistics/rx_packets`
  PT1=`cat /sys/class/net/$INTERFACE/statistics/tx_packets`
  # Wait one sec
  sleep $INTERVAL
  # Store second values of Bytes and packets
  R2=`cat /sys/class/net/$INTERFACE/statistics/rx_bytes`
  T2=`cat /sys/class/net/$INTERFACE/statistics/tx_bytes`

  PR2=`cat /sys/class/net/$INTERFACE/statistics/rx_packets`
  PT2=`cat /sys/class/net/$INTERFACE/statistics/tx_packets`

  # Subtract values and get byterate/sec
  TBPS=$(($T2-$T1))
  RBPS=$(($R2-$R1))
  # Count Bitrate/sec from Byterate/sec
  TbPS=$(($TBPS*8))
  RbPS=$(($RBPS*8))
  # Subtract values and get packetrate/sec
  TXPPS=$(($PT2 - $PT1))
  RXPPS=$(($PR2 - $PR1))

  # IF $HTTP value is true then - try to get HTTP metrics from the $LOG_FILE
  if $HTTP
  then
    http_total_request_size=$(grep $now_date $LOG_FILE | awk '{sum += $11} END {print sum}')
    http_total_response_size=$(grep $now_date $LOG_FILE | awk '{sum += $12} END {print sum}')
    http_total_request_time=$(grep $now_date $LOG_FILE | awk '{sum += $14} END {print sum}')
    http_num_requests=$(grep $now_date $LOG_FILE | wc -l)
  fi


  # IF $http_num_requests IS greater than 0 then count the averages of HTTP metrics
  if [[ $http_num_requests -gt 0 ]]; then
    http_avg_request_size=$((http_total_request_size / http_num_requests))
    http_avg_response_size=$((http_total_response_size / http_num_requests))
    http_avg_response_time=$((http_total_request_time / http_num_requests))
  else
    http_avg_request_size=0
    http_avg_response_size=0
    http_num_requests=0
    http_avg_response_time=0
  fi

  # Get CPU, RAM and TCP values
  cpu_use=`top -b -n 1 | grep Cpu | cut -d ':' -f2 | awk '{print $1}'`
  mem_use=`free -m | grep "Mem" | awk '{print $3/$2*100}'`
  tcp_est=`netstat -tan | grep ESTABLISHED| wc -l`
  syn_recv=`netstat -an | grep SYN_RECV | wc -l`
  syn_sent=`netstat -an | grep SYN_SENT | wc -l`
  fin_wait=`netstat -an | grep FIN_WAIT | wc -l`
  time_wait=`netstat -an | grep TIME_WAIT | wc -l`
  closing=`netstat -an | grep CLOSING | wc -l`
  close_wait=`netstat -an | grep CLOSE_WAIT | wc -l`

  # Print values to console
  echo "cpu: $cpu_use;ram: $mem_use;btx: $TbPS;brx: $RbPS;ptx: $TXPPS;prx:$RXPPS;tcps:$tcp_est;syn-recv:$syn_recv;syn-sent:$syn_sent;fin-wait:$fin_wait;time-wait:$time_wait;closing:$closing;close_wait:$close_wait;http_avg_req_size:$http_avg_request_size;http_avg_res_size:$http_avg_response_size;http_req:$http_num_requests;http_avg_res_time:$http_avg_response_time"

  # Try to create a new database
  sqlite3 $DB_FILE <<EOF
    CREATE TABLE IF NOT EXISTS data (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      cpu REAL NOT NULL,
      ram REAL NOT NULL,
      bit_rate_in INTEGER NOT NULL,
      bit_rate_out INTEGER NOT NULL,
      packet_rate_in INTEGER NOT NULL,
      packet_rate_out INTEGER NOT NULL,
      tcp_established INTEGER NOT NULL,
      tcp_syn_recv INTEGER NOT NULL,
      tcp_syn_sent INTEGER NOT NULL,
      tcp_fin_wait INTEGER NOT NULL,
      tcp_time_wait INTEGER NOT NULL,
      tcp_close_wait INTEGER NOT NULL,
      tcp_closing INTEGER NOT NULL,
      http_avg_req_size INTEGER NOT NULL,
      http_avg_res_size REAL NOT NULL,
      http_req REAL NOT NULL,
      http_avg_res_time REAL NOT NULL,
      timestamp INTEGER NOT NULL);
EOF
  # Save data to database
  sqlite3 $DB_FILE "INSERT INTO data(cpu, ram, bit_rate_in, bit_rate_out, packet_rate_in, packet_rate_out, tcp_established, tcp_syn_recv, tcp_syn_sent, tcp_fin_wait, tcp_time_wait, tcp_close_wait, tcp_closing, http_avg_req_size, http_avg_res_size, http_req, http_avg_res_time, timestamp) VALUES (\"$cpu_use\", \"$mem_use\", \"$TbPS\", \"$RbPS\", \"$TXPPS\", \"$RXPPS\", \"$tcp_est\", \"$syn_recv\", \"$syn_sent\", \"$fin_wait\", \"$time_wait\", \"$close_wait\", \"$closing\",\"$http_avg_request_size\", \"$http_avg_response_size\", \"$http_num_requests\", \"$http_avg_response_time\",  strftime('%s'));"
}

# Never ending loop
while [ true ]; do
    get_data
done

