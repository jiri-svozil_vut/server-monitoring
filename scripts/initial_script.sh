#!/usr/bin bash

apt-get update
apt install -y sqlite3 nodejs npm
timedatectl set-timezone Europe/Prague
npm install
chmod +x /var/server-monitoring/server.js
chmod +x /var/server-monitoring/scripts/server-monitoring.sh
cat /var/server-monitoring/scripts/service.txt > /lib/systemd/system/server-monitoring.service
cat /var/server-monitoring/node-service.txt > /lib/systemd/system/server-monitoring-api.service
systemctl daemon-reload

systemctl enable server-monitoring.service
systemctl enable server-monitoring-api.service

systemctl start server-monitoring.service
systemctl start server-monitoring-api.service
